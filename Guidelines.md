# Word Pair Relatedness Annotation Guidelines

## Task
You will be presented with a list of word pairs (word1 and word2). You are asked to assign a score between 0 and 10 to each pair based on how related they are.

## Scoring
The score of a word to itself should always be 10. You can use fractions, e.g. 7.5, if you need.
If you are unsure what a word means, you can consult a dictionary or a native speaker of English.
Please try to score all pairs. However, if it is impossible for you to assign a score, leave the scoring field blank.

## Relatedness explained
Consider two words to be related if:
* They are synonyms: pretty - attractive
* They are in a part-of relation: body - hand
* They are antonyms (opposites): dead - alive
* They are linked by function or other kinds of association: electricity - oven, ocean - cruise, ...
